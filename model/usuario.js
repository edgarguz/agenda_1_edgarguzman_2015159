var database = require('./database');
var usuario = {};

usuario.selectAll = function(cualquierFuncionCallback){
	if(database){
		database.query("SELECT * FROM Usuario",
			function(error, resultados){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, resultados);
				}
			});
	}
}

usuario.select = function(idUsuario, cualquierFuncionCallback){
	if(database){
		var consulta = "SELECT * FROM Usuario WHERE idUsuario = ?";
		database.query(consulta, idUsuario,
			function(error, resultado){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, resultado);
				}
			});
	}
}

usuario.autenticar = function(data, cualquierFuncionCallback){
	if(database){
		var consulta = "SELECT * FROM Usuario WHERE nick = ? AND contrasena = ?";
		database.query(consulta, [data.nick, data.contrasena],
			function(error, resultado){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, resultado);
				}
			});
	}
}
usuario.insert = function(data, cualquierFuncionCallback){
	if(database){
		database.query("INSERT INTO USUARIO SET ?",data,
		function(error, resultado){
				if(error){
					throw error;
				}else{
					var respuesta={
						insertId: resultado.insertId,
					  nick:data.nick,
				 	  idUsuario: resultado.insertId
					}
					cualquierFuncionCallback(null, respuesta);
				}
			});
	}
}

usuario.update = function(data, cualquierFuncionCallback){
	if(database){
		var sql = "call editarUsuario(?, ?, ?)"
		database.query(sql, [data.idUsuario, data.nick, data.contrasena],
			 function(error, resultado){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, data);
				}
			});
	}
}


usuario.delete = function(idUsuario, cualquierFuncionCallback){
	if(database){
		var consulta = "call eliminarUsuario (?)";
		database.query(consulta, idUsuario,
			function(error, resultado){
				if(error){
					throw error;
				}else{
					var notificacion = {"Mensaje": ""}
					if(resultado.affectedRows > 0){
						notificacion.Mensaje = "C elimino el usuario";
					}else{
						notificacion.Mensaje = "No c elimino el usuario";
					}

					cualquierFuncionCallback(null, notificacion)

				}
			});
	}
}


usuario.autenticar2 = function(data, cualquierFuncionCallback){
	if(database){
		var consulta = "SELECT * FROM Usuario WHERE nick = ? AND contrasena = ?";
		database.query(consulta, [data.nick, data.contrasena],
			function(error, resultado){
				if(error){
					throw error;
				}else{
					var inicioCorrecto = false;
					if(resultado.length > 0)
										{
											inicioCorrecto = true;
										}

					cualquierFuncionCallback(null, {"inicioCorrecto": inicioCorrecto});
				}
			});
	}
}
module.exports = usuario;
