var database = require('./database');
var contacto = {};

contacto.selectAll = function(idUsuario, cualquierFuncionCallback){
	if(database){
		database.query("SELECT * FROM v_contacto where idUsuario  = ?",
			idUsuario,
			function(error, resultados){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, resultados);
				}
			});
	}
}

contacto.select = function(idContacto, cualquierFuncionCallback){
	if(database){
		var consulta = "SELECT * FROM Contacto WHERE idContacto = ?";
		database.query(consulta, idContacto,
			function(error, resultado){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, resultado);
				}
			});
	}
}

contacto.insert = function(data, cualquierFuncionCallback){
	if(database){
		database.query("call agregarContacto(?,?,?,?,?,?,?)",
			[data.nombre, data.apellido, data.direccion, data.telefono, data.correo, data.idCategoria, data.idUsuario], function(error, resultado){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, {"insertId": resultado.insertId});
				}
			});
	}
}

contacto.update = function(data, cualquierFuncionCallback){
	if(database){
		var sql = "call editarContacto(?,?,?,?,?,?,?)"
		database.query(sql, [data.idContacto, data.nombre, data.apellido, data.direccion, data.telefono, data.correo, data.idCategoria],
			 function(error, resultado){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, data);
				}
			});
	}
}


contacto.delete = function(idContacto, cualquierFuncionCallback){
	if(database){
		var consulta = "call eliminarContacto (?)";
		database.query(consulta, idContacto,
			function(error, resultado){
				if(error){
					throw error;
				}else{
					var notificacion = {"Mensaje": ""}
					if(resultado.affectedRows > 0){
						notificacion.Mensaje = "C elimino el contacto";
					}else{
						notificacion.Mensaje = "No c elimino el contacto";
					}

					cualquierFuncionCallback(null, notificacion)

				}
			});
	}
}
module.exports = contacto;
