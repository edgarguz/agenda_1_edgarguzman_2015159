var database = require('./database');
var detalleUsuario = {};

detalleUsuario.selectAll = function(cualquierFuncionCallback){
	if(database){
		database.query("SELECT * FROM DetalleUsuario",
			function(error, resultados){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, resultados);
				}
			});
	}
}

detalleUsuario.select = function(idDetalleUsuario, cualquierFuncionCallback){
	if(database){
		var consulta = "SELECT * FROM DetalleUsuario WHERE idDetalleUsuario = ?";
		database.query(consulta, idDetalleUsuario,
			function(error, resultado){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, resultado);
				}
			});
	}
}

detalleUsuario.insert = function(data, cualquierFuncionCallback){
	if(database){
		database.query("call agregarDetalleUsuario(?,?)",
  			[ data.idUsuario, data.idContacto], function(error, resultado){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, {"insertId": resultado.insertId});
				}
			});
	}
}

detalleUsuario.update = function(data, cualquierFuncionCallback){
	if(database){
		var sql = "UPDATE UsuarioDetalle SET idUsuario = ?, idContacto = ? WHERE idUsuarioDetalle = ?"
		database.query(sql, [data.idUsuario, data.idContacto, data.idUsuarioDetalle],
			 function(error, resultado){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, data);
				}
			});
	}
}


detalleUsuario.delete = function(idDetalleUsuario, cualquierFuncionCallback){
	if(database){
		var consulta = "DELETE FROM UsuarioDetalle WHERE idUsuarioDetalle = ?";
		database.query(consulta, idDetalleUsuario,
			function(error, resultado){
				if(error){
					throw error;
				}else{
					var notificacion = {"Mensaje": ""}
					if(resultado.affectedRows > 0){
						notificacion.Mensaje = "C elimino el detalleUsuario";
					}else{
						notificacion.Mensaje = "No c elimino el detalleUsuario";
					}

					cualquierFuncionCallback(null, notificacion)

				}
			});
	}
}
module.exports = detalleUsuario;
