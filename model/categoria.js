var database = require('./database');
var categoria = {};

categoria.selectAll = function(cualquierFuncionCallback){
	if(database){
		database.query("SELECT * FROM Categoria",
			function(error, resultados){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, resultados);
				}
			});
	}
}

categoria.select = function(idCategoria, cualquierFuncionCallback){
	if(database){
		var consulta = "SELECT * FROM Categoria WHERE idCategoria = ?";
		database.query(consulta, idCategoria,
			function(error, resultado){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, resultado);
				}
			});
	}
}

categoria.insert = function(data, callback) {
  if(database) {
    var consulta = "call agregarCategoria(?)";
    database.query(consulta, [data.nombreCategoria], function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });
  }
}

categoria.update = function(data, cualquierFuncionCallback){
	if(database){
		var sql = "UPDATE Categoria SET nombreCategoria = ? WHERE idCategoria = ?"
		database.query(sql, [data.nombreCategoria, data.idCategoria],
			 function(error, resultado){
				if(error){
					throw error;
				}else{
					cualquierFuncionCallback(null, data);
				}
			});
	}
}


categoria.delete = function(idCategoria, cualquierFuncionCallback){
	if(database){
		var consulta = "DELETE FROM Categoria WHERE idCategoria = ?";
		database.query(consulta, idCategoria,
			function(error, resultado){
				if(error){
					throw error;
				}else{
					var notificacion = {"Mensaje": ""}
					if(resultado.affectedRows > 0){
						notificacion.Mensaje = "C elimino la categoria";
					}else{
						notificacion.Mensaje = "No c elimino la categoria";
					}

					cualquierFuncionCallback(null, notificacion)

				}
			});
	}
}
module.exports = categoria;
