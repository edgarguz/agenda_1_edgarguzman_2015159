CREATE DATABASE agendain6am2;
USE  agendain6am2;


CREATE TABLE Categoria(
	idCategoria int not null auto_increment,
    nombreCategoria varchar(30) not null,
    primary key (idCategoria)
)ENGINE=InnoDB;

CREATE TABLE Contacto(
	idContacto INT NOT NULL auto_increment,
    nombre varchar(30) not null,
    apellido varchar(30) not null,
    direccion varchar(30) not null,
    telefono varchar(12) not null,
    correo varchar(40) not null,
    idCategoria int not null,
    primary key (idContacto),
    foreign key (idCategoria) references Categoria(idCategoria)
)ENGINE=InnoDB;

create table Usuario(
	idUsuario int not null auto_increment,
    nick varchar(30) not null,
    contrasena varchar(30) not null,
    primary key (idUsuario)
)ENGINE=InnoDB;

create table UsuarioDetalle(
	idUsuarioDetalle int not null auto_increment,
    idUsuario int not null,
    idContacto int not null,
    primary key (idUsuarioDetalle),
    foreign key (idUsuario) references Usuario(idUsuario) ON DELETE CASCADE,
    foreign key (idContacto) references Contacto(idContacto) ON DELETE CASCADE
)ENGINE=InnoDB;

CREATE TABLE Historial(
	idHistorial INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	descripcion VARCHAR(100) not null,
	fecha datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
	idUsuario int not null,
	foreign key (idUsuario) references Usuario(idUsuario) ON DELETE CASCADE 
)ENGINE=InnoDB;

-- TRIGGER
DELIMITER @@
CREATE TRIGGER registroContacto AFTER INSERT ON UsuarioDetalle
  FOR EACH ROW
  BEGIN
    DECLARE _idUsuario integer;
    SET _idUsuario = (SELECT idUsuario from UsuarioDetalle ORDER BY idUsuarioDetalle DESC LIMIT 1);
    IF (_idUsuario <> 0) THEN
       INSERT INTO Historial(descripcion, idUsuario) VALUES ('Ha agregado un contacto', _idUsuario);
    END IF;
  END;
@@

-- usuario
DROP PROCEDURE IF EXISTS agregarUsuario;
DELIMITER $$
CREATE PROCEDURE agregarUsuario(IN _nick VARCHAR(30), IN _contrasena VARCHAR(30)) BEGIN
INSERT INTO Usuario(nick, contrasena) VALUES (_nick, _contrasena); END
$$

DROP PROCEDURE IF EXISTS editarUsuario;
DELIMITER $$
CREATE PROCEDURE editarUsuario(IN _idUsuario INT, IN _nick VARCHAR(30), IN _contrasena VARCHAR(30)) BEGIN
UPDATE Usuario SET nick = _nick, contrasena = _contrasena WHERE idUsuario = _idUsuario; 
INSERT INTO Historial(descripcion, idUsuario) VALUES ('Ha editado su nick o contraseña', _idUsuario);END
$$

DROP PROCEDURE IF EXISTS eliminarUsuario;
DELIMITER $$
CREATE PROCEDURE eliminarUsuario(IN _idUsuario INT) BEGIN
DELETE FROM Usuario WHERE idUsuario = _idUsuario; END
$$

-- CATEGORIA
DROP PROCEDURE IF EXISTS agregarCategoria;
DELIMITER $$
CREATE PROCEDURE agregarCategoria(IN _nombreCategoria VARCHAR(30)) BEGIN
INSERT INTO Categoria(nombreCategoria) VALUES (_nombreCategoria); END
$$

-- CONTACTO
DROP PROCEDURE IF EXISTS agregarContacto;
DELIMITER $$
CREATE PROCEDURE agregarContacto(IN _nombre VARCHAR(30), IN _apellido VARCHAR(30), IN _direccion VARCHAR(30), IN _telefono VARCHAR(12), IN _correo VARCHAR(40), IN _idCategoria INT, IN _idUsuario INT) BEGIN DECLARE _idNuevoContacto INT DEFAULT 0;
INSERT INTO Contacto(nombre, apellido, direccion, telefono, correo, idCategoria) VALUES (_nombre, _apellido, _direccion, _telefono, _correo, _idCategoria); 
SET _idNuevoContacto = (SELECT MAX(idContacto) FROM Contacto);
INSERT INTO UsuarioDetalle(idUsuario, idContacto) VALUES (_idUsuario, _idNuevoContacto); END
$$

DROP PROCEDURE IF EXISTS editarContacto;
DELIMITER $$
CREATE PROCEDURE editarContacto(IN _idContacto INT, IN _nombre VARCHAR(30), IN _apellido VARCHAR(30), IN _direccion VARCHAR(30), IN _telefono VARCHAR(12), IN _correo VARCHAR(40), IN _idCategoria INT) BEGIN
UPDATE Contacto SET nombre = _nombre, apellido = _apellido, direccion = _direccion, telefono= _telefono, correo = _correo, idCategoria=_idCategoria WHERE idContacto = _idContacto; END
$$

DROP PROCEDURE IF EXISTS eliminarContacto;
DELIMITER $$
CREATE PROCEDURE eliminarContacto(IN _idContacto INT) BEGIN
DELETE FROM Contacto WHERE idContacto = _idContacto; END
$$


-- detalle Usuario
DROP PROCEDURE IF EXISTS agregarDetalleUsuario;
DELIMITER $$
CREATE PROCEDURE agregarDetalleUsuario(IN _idUsuario INT, IN _idContacto INT) BEGIN
INSERT INTO UsuarioDetalle(idUsuario, idContacto) VALUES (_idUsuario, _idContacto); END
$$

DROP PROCEDURE IF EXISTS eliminarDetalleUsuario;
DELIMITER $$
CREATE PROCEDURE eliminarDetalleUsuario(IN _idDetalleUsuario INT) BEGIN
DELETE FROM UsuarioDetalle WHERE idUsuarioDetalle = _idDetalleUsuario; END
$$

CREATE VIEW v_contacto
AS
SELECT u.idContacto, u.idUsuario, c.nombre, c.apellido, c.direccion, c.telefono, c.correo, cat.nombreCategoria as Categoria
FROM  UsuarioDetalle u
INNER JOIN Contacto c ON u.idContacto = c.idContacto
INNER JOIN  Categoria cat on c.idCategoria = cat.idCategoria


INSERT INTO Categoria(nombreCategoria)
VALUES('Default');

select * from Usuario;