
var Index = function(){
  var main = this;
  var indexUri = "http://localhost:3000/sesion";

  main.usuarioLogeado = {
    idUsuarioLogeado: ko.observable(),
    nickUsuarioLogeado: ko.observable()
  }


  function ajaxHelper(uri, method, data) {
      return $.ajax({
        url : uri,
        type: method,
        dataType: 'json',
        contentType: 'application/json',
        data: data ? JSON.stringify(data) : null
      }).fail(function(jqXHR, textStatus, errorThrown){
        console.log(errorThrown);
      })
    }

  main.getUsuarioLogeado = function(){
    ajaxHelper(indexUri, 'GET').done(function(data){
      main.usuarioLogeado.idUsuarioLogeado(data.id);
      main.usuarioLogeado.nickUsuarioLogeado("Bienvenido " +data.nick) ;
    })
  }

  main.getUsuarioLogeado();
}

$(document).ready(function(){
  var index = new Index();
  ko.applyBindings(index);
});
