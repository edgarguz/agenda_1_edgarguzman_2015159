
var ContactoCliente = function(){
  var main = this;
  var contactoUri = "http://localhost:3000/api/contacto";
  var categoriaUri = "http://localhost:3000/api/categoria";

  main.contactos = ko.observableArray([]);
  main.categorias = ko.observableArray([]);

  main.contactoCargado = ko.observable();

  main.categoriaSeleccionada = ko.observable();
  main.categoriaSeleccionada2 = ko.observable();

  main.nuevoContacto = {
    nombre: ko.observable(),
    apellido: ko.observable(),
    direccion: ko.observable(),
    telefono: ko.observable(),
    correo: ko.observable(),
    idCategoria: ko.observable()
  }

  function ajaxHelper(uri, method, data) {
      return $.ajax({
        url : uri,
        type: method,
        dataType: 'json',
        contentType: 'application/json',
        data: data ? JSON.stringify(data) : null
      }).fail(function(jqXHR, textStatus, errorThrown){
        console.log(errorThrown);
      })
    }

  main.agregar = function(){
    var contacto = {
      nombre:  main.nuevoContacto.nombre(),
      apellido: main.nuevoContacto.apellido(),
      direccion: main.nuevoContacto.direccion(),
      telefono: main.nuevoContacto.telefono(),
      correo: main.nuevoContacto.correo(),
      idCategoria: main.categoriaSeleccionada2().idCategoria
    }
     ajaxHelper(contactoUri, 'POST', contacto).done(
       function(item){
         main.getAllContactos();
       })
  }
  main.editar = function(){
   var data = {
     idContacto: main.contactoCargado().idContacto,
     nombre: main.contactoCargado().nombre,
     apellido: main.contactoCargado().apellido,
     direccion: main.contactoCargado().direccion,
     telefono: main.contactoCargado().telefono,
     correo: main.contactoCargado().correo,
     idCategoria: main.categoriaSeleccionada().idCategoria
   }
   ajaxHelper(contactoUri + "/" + data.idContacto,'PUT',data).done(
     function(item){
       Materialize.toast('Datos actualizados con exito!', 8000);
       main.getAllContactos();
     }
    )
  }

  main.eliminar = function(item){
    var contactoId = item.idContacto;
    console.log(JSON.stringify(item));
      ajaxHelper(contactoUri +"/"+ contactoId, 'DELETE').done(function(data){
        main.getAllContactos();
      })
  }

  main.getAllContactos = function(){
    ajaxHelper(contactoUri, 'GET').done(function(data){
      main.contactos(data);
    })
  }

  main.getAllCategorias = function(){
    ajaxHelper(categoriaUri, 'GET').done(function(data){
      main.categorias(data);
    })
  }

  main.cargar = function(item){
    console.log(JSON.stringify(item));
       main.contactoCargado(item);
  }



  main.getAllContactos();
  main.getAllCategorias();
}

$(document).ready(function(){
  var contacto = new ContactoCliente();
  ko.applyBindings(contacto);
});
