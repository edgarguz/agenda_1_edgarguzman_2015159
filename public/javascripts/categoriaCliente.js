
var CategoriaCliente = function(){
  var main = this;
  var categoriaUri = "http://localhost:3000/api/categoria";

  main.categorias = ko.observableArray([]);

  main.categoriaCargada = ko.observable();

  main.nuevaCategoria = {
    nombreCategoria: ko.observable()
  }

  function ajaxHelper(uri, method, data) {
      return $.ajax({
        url : uri,
        type: method,
        dataType: 'json',
        contentType: 'application/json',
        data: data ? JSON.stringify(data) : null
      }).fail(function(jqXHR, textStatus, errorThrown){
        console.log(errorThrown);
      })
    }

  main.agregar = function(){
   	var categoria = {
   		nombreCategoria : main.nuevaCategoria.nombreCategoria()
   	}
   	ajaxHelper(categoriaUri, 'POST', categoria).done(function(item){
   		main.getAllCategorias();
   	})
   }

  main.editar = function(){
   var data = {
     idCategoria: main.categoriaCargada().idCategoria,
     nombreCategoria: main.categoriaCargada().nombreCategoria
   }
   ajaxHelper(categoriaUri + "/" + data.idCategoria,'PUT',data).done(
     function(item){
       Materialize.toast('Datos actualizados con exito!', 8000);
       main.getAllCategorias();
     }
    )
  }



 main.eliminar = function(item){
   var CategoriaId = item.idCategoria;
   console.log(JSON.stringify(item));
   if(CategoriaId !== 1){
     ajaxHelper(categoriaUri +"/"+ CategoriaId, 'DELETE').done(function(data){
       main.getAllCategorias();
     })
   }else{
     Materialize.toast('No puedes eliminar esta categoria', 4000);
   }
 }



  main.cargar = function(item){
  	console.log(JSON.stringify(item));
    if(item.idCategoria !== 1){
  	   main.categoriaCargada(item);
    }else{
      Materialize.toast('No puedes editar esta categoria', 4000)
    }
  }

  main.getAllCategorias = function(){
    ajaxHelper(categoriaUri, 'GET').done(function(data){
      main.categorias(data);
    })
  }

  main.getAllCategorias();
}

$(document).ready(function(){
  var categoria = new CategoriaCliente();
  ko.applyBindings(categoria);
});
