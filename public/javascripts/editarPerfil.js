
var Perfil = function(){
  var main = this;
  var sesionUri = "http://localhost:3000/sesion";
  var usuarioUri = "http://localhost:3000/api/usuario";
  var loginUri="http://localhost:3000/autenticar";
  var salirUri="http://localhost:3000/salir";
  main.usuarioLogeado = {
    idUsuarioLogeado: ko.observable(),
    nickUsuarioLogeado: ko.observable()
  }
  main.nuevoUsuario = {
    nick: ko.observable(),
    contrasena: ko.observable()
  }
  main.editar = function(){
    var data = {
      idUsuario: main.usuarioLogeado.idUsuarioLogeado(),
      nick:  main.nuevoUsuario.nick(),
      contrasena:   main.nuevoUsuario.contrasena()
    }
    ajaxHelper(usuarioUri + "/" + data.idUsuario,'PUT',data).done(
      function(item){
        Materialize.toast('Datos actualizados con exito!', 8000);
        main.actualizar();
      }
    )
  }
  main.actualizar = function(){
    var data = {
      nick:  main.nuevoUsuario.nick(),
      contrasena:   main.nuevoUsuario.contrasena()
    }
    ajaxHelper(loginUri, 'POST', data).done(
      function(item){

      }
    )
  }
  main.eliminarCuenta = function(){
    var idUsuario = main.usuarioLogeado.idUsuarioLogeado();
    Materialize.toast(idUsuario, 4000);
    ajaxHelper(usuarioUri +"/"+ idUsuario, 'DELETE').done(function(data){
      ajaxHelper(salirUri, 'GET').done(function(data){
          // hacer que vuelva al index
      })
    })
  }

  function ajaxHelper(uri, method, data) {
      return $.ajax({
        url : uri,
        type: method,
        dataType: 'json',
        contentType: 'application/json',
        data: data ? JSON.stringify(data) : null
      }).fail(function(jqXHR, textStatus, errorThrown){
        console.log(errorThrown);
      })
    }

  main.getUsuarioLogeado = function(){
    ajaxHelper(sesionUri, 'GET').done(function(data){
      main.usuarioLogeado.idUsuarioLogeado(data.id);
      main.usuarioLogeado.nickUsuarioLogeado(data.nick);
      main.nuevoUsuario.nick(data.nick);
    })
  }

  main.getUsuarioLogeado();
}

$(document).ready(function(){
  var editarPerfil = new Perfil();
  ko.applyBindings(editarPerfil);
});
