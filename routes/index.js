var express = require('express');
var router = express.Router();
var Autenticacion = require('../helper/autenticacion');
var auth = new Autenticacion();


/* GET home page. */
router.get('/', function(req, res, next) {
  auth.autorizar(req);
	res.render(auth.getPath() + 'index');
});
router.get('/cookie/clear', function(req, res){
	res.clearCookie('nick');
	res.clearCookie('idUsuario');
	res.end("Se eliminaron las cookies");
});
// datos del usuario loggeado
router.get('/sesion', function(req, res){
    var usuario = {
      id: auth.getIdUsuario() ,
      nick: auth.getNick()
    }
    res.json(usuario);
});

router.get('/cookie/all', function(req, res){
	res.status(200).send(req.cookies);
});


module.exports = router;
