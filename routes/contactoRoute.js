var express = require('express');
var contacto = require('../model/contacto');
var router = express.Router();
var Autenticacion = require('../helper/autenticacion');
var auth = new Autenticacion();

router.get('/contacto', function(req, res){
	res.render('dashboard/contactos', {title: 'Contactos'});
});

router.get('/api/contacto', function(req, res){
	auth.autorizar(req);
	if(auth.getAcceso()){
		contacto.selectAll(auth.getIdUsuario(),function(error, resultados){
			if(typeof resultados !== undefined){
				res.json(resultados);
			}else{
				res.json({"Mensaje" : "No hay contactos"});
			}
		});
	}else{
		console.log('no se pudo, regresa a autenticar');
		res.redirect('/autenticar');
	}

});

router.get('/api/contacto/:idContacto', function(req, res){
	var idContacto = req.params.idContacto;

	contacto.select(idContacto, function(error, resultado){
		if(typeof resultado !== undefined){
			res.json(resultado);
		}else{
			res.json({"Mensaje" : "No hay contacto"});
		}
	});
});

router.post('/api/contacto', function(req, res){
	var data ={
		nombre: req.body.nombre,
    apellido: req.body.apellido,
    direccion: req.body.direccion,
    telefono: req.body.telefono,
    correo: req.body.correo,
    idCategoria: req.body.idCategoria,
    idUsuario: auth.getIdUsuario() // obtener usuario loggeado
	}
	console.log("Id usuario loggeado: "+data.idUsuario);

	contacto.insert(data,function(error, resultado){
		if(resultado && resultado.insertId > 0){
			res.json(resultado);
		}else{
			res.json({"Mensaje" : "No hay contactos"});
		}
	});
});

router.put('/api/contacto/:idContacto', function(req, res){
	var idContacto = req.params.idContacto;
	var data = {
		idContacto: req.body.idContacto,
		nombre: req.body.nombre,
    apellido: req.body.apellido,
    direccion: req.body.direccion,
    telefono: req.body.telefono,
    correo: req.body.correo,
    idCategoria: req.body.idCategoria
  	}
	if(idContacto == data.idContacto){
		console.log("Logra ingresar al model");
		contacto.update(data,function(error, resultado){
			if(resultado !== undefined){
				res.json(resultado);
			}else{
				res.json({"Mensaje": "No c modifico el contacto"});
			}
		});
	}else{
		res.json({"Mensaje":"No concuerdan los id's "});
	}
});


router.delete('/api/contacto/:idContacto', function(req, res){
	var idContacto = req.params.idContacto;

		contacto.delete(idContacto,function(error, resultado){
			if(typeof resultado !== undefined){
				res.json(resultado);
			}else{
				res.json({"Mensaje": "No c elimino el contacto"});
			}
		});
});
module.exports = router;
