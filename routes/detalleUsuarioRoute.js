var express = require('express');
var detalleUsuario = require('../model/detalleUsuario');
var router = express.Router();

router.get('/api/detalleUsuario', function(req, res){
	detalleUsuario.selectAll(function(error, resultados){
		if(typeof resultados !== undefined){
			res.json(resultados);
		}else{
			res.json({"Mensaje" : "No hay detalles"});
		}
	});
});

router.get('/api/detalleUsuario/:idDetalleUsuario', function(req, res){
	var idDetalleUsuario = req.params.idDetalleUsuario;

	detalleUsuario.select(idDetalleUsuario, function(error, resultado){
		if(typeof resultado !== undefined){
			res.json(resultado);
		}else{
			res.json({"Mensaje" : "No hay detalle"});
		}
	});
});

router.post('/api/detalleUsuario', function(req, res){
	var data ={
		idUsuario: req.body.idUsuario,
    idContacto: req.body.idContacto
	}
	detalleUsuario.insert(data,function(error, resultado){
		if(resultado && resultado.insertId > 0){
			var idDetalleUsuario = resultado.insertId;
			res.redirect("/api/detalleUsuario/"+idDetalleUsuario);
		}else{
			res.json({"Mensaje" : "No hay detalleUsuarios"});
		}
	});
});

router.put('/api/detalleUsuario/:idDetalleUsuario', function(req, res){
	var idDetalleUsuario = req.params.idDetalleUsuario;
	var data = {
		idUsuarioDetalle: req.body.idDetalleUsuario,
    idUsuario: req.body.idUsuario,
    idContacto: req.body.idContacto
	}
	if(idDetalleUsuario === data.idUsuarioDetalle){
		detalleUsuario.update(data,function(error, resultado){
			if(resultado !== undefined){
				res.redirect("/api/detalleUsuario");
			}else{
				res.json({"Mensaje": "No c modifico la detalleUsuario"});
			}
		});
	}else{
		res.json({"Mensaje":"No concuerdan los id's "});
	}
});

router.delete('/api/detalleUsuario/:idDetalleUsuario', function(req, res){
	var idDetalleUsuario = req.params.idDetalleUsuario;

		detalleUsuario.delete(idDetalleUsuario,function(error, resultado){
			if(typeof resultado !== undefined){
				res.json(resultado);
			}else{
				res.json({"Mensaje": "No c elimino el detalle"});
			}
		});
});
module.exports = router;
