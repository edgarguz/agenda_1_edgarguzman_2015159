var express = require('express');
var usuario = require('../model/usuario');
var router = express.Router();

router.get('/perfil', function(req, res){
	res.render('dashboard/editarPerfil', {title: 'Registrar'});
});
router.get('/registrar', function(req, res){
	res.render('default/registrar', {title: 'Registrar'});
});
router.get('/autenticar', function(req, res) {
  res.render('default/autenticar', {title: 'Autenticar'});
});
router.get('/salir', function(req, res){
	res.clearCookie('idUsuario');
  res.clearCookie('nick');
  res.redirect('/');
});


router.post('/autenticar', function(req, res){
	var data ={
		nick: req.body.nick,
    contrasena:req.body.contrasena
	}
	usuario.autenticar(data, function(error, resultado){
		if(resultado[0] !== undefined){
			res.cookie('nick', resultado[0].nick);
			res.cookie('idUsuario', resultado[0].idUsuario);
			res.redirect('/');
		}else{
			res.redirect('/');
		}
	});
});

router.get('/api/usuario', function(req, res){
	usuario.selectAll(function(error, resultados){
		if(typeof resultados !== undefined){
			res.json(resultados);
		}else{
			res.json({"Mensaje" : "No hay usuarios"});
		}
	});
});
router.get('/api/usuario/:idUsuario', function(req, res){
	var idUsuario = req.params.idUsuario;

	usuario.select(idUsuario, function(error, resultado){
		if(typeof resultado !== undefined){
			res.json(resultado);
		}else{
			res.json({"Mensaje" : "No hay usuario"});
		}
	});
});

router.post('/registrar', function(req, res){
	var data ={
		nick: req.body.nick,
    contrasena: req.body.contrasena
	}
	usuario.insert(data,function(error, resultado){
		if(resultado && resultado.insertId > 0){
			res.cookie('nick', resultado.nick);
      res.cookie('idUsuario', resultado.idUsuario);
      res.redirect('/');
		}else{
			res.json({"Mensaje" : "No hay usuarios"});
		}
	});
});






router.put('/api/usuario/:idUsuario', function(req, res){
	var idUsuario = req.params.idUsuario;
	var data = {
		idUsuario: req.body.idUsuario,
		nick: req.body.nick,
    contrasena: req.body.contrasena
	}
	if(idUsuario === data.idUsuario){
		usuario.update(data,function(error, resultado){
			if(resultado !== undefined){
				res.json(resultado);
			}else{
				res.json({"Mensaje": "No c modifico el usuario"});
			}
		});
	}else{
		res.json({"Mensaje":"No concuerdan los id's "});
	}
});




router.delete('/api/usuario/:idUsuario', function(req, res){
	var idUsuario = req.params.idUsuario;

		usuario.delete(idUsuario,function(error, resultado){
			if(typeof resultado !== undefined){
				res.json(resultado);
			}else{
				res.json({"Mensaje": "No c elimino el usuario"});
			}
		});
});





module.exports = router;
